import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

import java.util.concurrent.TimeUnit;

public class PracticeExamples {

    public WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testCartExample() throws InterruptedException {
//  Going to the pragmatic shop page
        driver.get("http://shop.pragmatic.bg/");

//  Write some random text in the search bar in http://shop.pragmatic.bg/
        WebElement searchBar = driver.findElement(By.cssSelector("div#search input.form-control"));
        searchBar.sendKeys("Blablablablabl");

//  Click on the search button
        driver.findElement(By.cssSelector("div#search span.input-group-btn button")).click();

//  Asserting the result of our search above - in this case it won't find anything based on the input "Blablablablabal"
        String actualText = driver.findElement(By.xpath("//input[@id='button-search']//following-sibling::p")).getText();
        assertEquals(actualText, "Your shopping cart is empty!");

//  Changing the search to something meaningful - in this case iPhone!
        WebElement searchBarNew = driver.findElement(By.id("input-search"));
        searchBarNew.clear();
        searchBarNew.sendKeys("iPhone");

//  Choosing the category Phones and PDAs from the dropDown menu next to the search bar!
        WebElement categoryDropDown = driver.findElement(By.xpath("//select[@name='category_id']"));
        Select dropDown = new Select(categoryDropDown);

        dropDown.selectByValue("24");
//        dropDown.selectByVisibleText("Phones & PDAs");

//  Checking the subcategory checkbox
        WebElement checkSubCategoryBut = driver.findElement(By.cssSelector("label.checkbox-inline input[name='sub_category']"));
        if (!checkSubCategoryBut.isSelected()) {
            checkSubCategoryBut.click();
        }

//  Checking the descriptions checkbox
        WebElement checkDescriptionBut = driver.findElement(By.id("description"));
        if (!checkDescriptionBut.isSelected()) {
            checkDescriptionBut.click();
        }

//  Clicking the search button
        driver.findElement(By.id("button-search")).click();

//  Asserting the result is the expected on according to our search.
        boolean isIphone = driver.findElement(By.linkText("iPhone")).isDisplayed();
        assertTrue(isIphone);

//  Checking that the Cart is empty before trying to put something in it
        WebElement cartElement = driver.findElement(By.cssSelector("div#cart button.dropdown-toggle"));
        cartElement.click();
        String actualTextCart = driver.findElement(By.cssSelector("p.text-center")).getText();
        assertEquals(actualTextCart, "Your shopping cart is empty!");

//  Adding the currently found product to the Cart
        driver.findElement(By.xpath("//div[@class='button-group']//button")).click();

//  Moving to the cart element in the top right corner
        Actions builder = new Actions(driver);
        builder.moveToElement(cartElement).perform();

//  Explicit wait for waiting before clicking on the updated cart!
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div#cart button.dropdown-toggle")));

//  Clicking on the updated cart!
        builder.click(cartElement).perform();

//  Asserting the added product quantity!
        String actualQtyText = driver.findElement(By.xpath("//table[@class='table table-striped']//td[3]")).getText();
        assertEquals(actualQtyText, "x 1");// you can write more asserts for exercise if you want

//  Clicking on the checkout and asserting the correct page was loaded
        driver.findElement(By.xpath("//p[@class='text-right']//i[@class='fa fa-share']//..")).click();
        assertEquals(driver.getTitle(), "Checkout");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}
